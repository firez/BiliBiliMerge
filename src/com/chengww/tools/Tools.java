package com.chengww.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import com.chengww.bean.Bean;
import com.google.gson.Gson;

public class Tools {

	/**
	 * 获取视频名字
	 * @param json
	 * @return
	 */
	public String[] json_getName(String json){
		Gson gson = new Gson();
		Bean bean = gson.fromJson(json,Bean.class);
		String[] names = null;
		if(bean.getPage_data() != null){
			Bean.PageDataBean pageData = bean.getPage_data();
			int page = pageData.getPage();
			String index = String.valueOf(page);
			if(page < 10){
				index = "0" + index;
			}
			//去除不符合命名规则的字符
			String name = nameMatch(pageData.getPart());
			names = new String[]{nameMatch(bean.getTitle()),index + "." + name};
		}else{
			Bean.EpBean ep = bean.getEp();
			String index = ep.getIndex();
			if(index.length() < 2){
				index = "0" + index;
			}
			String name = nameMatch(ep.getIndex_title());
			names = new String[]{nameMatch(bean.getTitle()),index + "." + name};
		}
		return names;
	}
	
	public boolean deleteFolder(File file){  
	    if(!file.exists()){  
	        return false;
	    }  
	    if(file.isFile() || file.listFiles().length == 0){  
	        file.delete();  
	        return true;  
	    }else{  
	        File[] files = file.listFiles();  
	        for(int i=0;i<files.length;i++){  
	            deleteFolder(files[i]);  
	        }  
	        file.delete();  
	        return true;
	    }
	}
	
	public void doMerge(File in,File out){
		//1、2、3、4...
		File[] files = in.listFiles();
		//输出路径
		String episode_path = null;
		//循环
		for(File f : files){
			//视频文件名和分P文件名-如第一话
			String[] names = null;
			//获得所有名为.blv的文件
			File[] ffs = null;
			File[] fs = f.listFiles();
			for(final File ff : fs){
				if(ff.getName().equals("entry.json")){
					String json_name = null;
					try {
						BufferedReader reader =  
						              new BufferedReader(new InputStreamReader(new FileInputStream(ff), Charset.forName("utf-8")));
						json_name = reader.readLine();
						reader.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					names = json_getName(json_name);
					
				}
				
				if(ff.isDirectory() && ff.getName().startsWith("lua.")){
					//重命名
					for(int i = 0; i < ff.list().length;i++){
						File pathname = ff.listFiles()[i];
						//0.blv -- 00.blv
						if(pathname.getName().endsWith(".blv") && pathname.getName().length() == 5){
							pathname.renameTo(new File(pathname.getParentFile().getAbsolutePath() + File.separator + "0" + pathname.getName()));
						}
						
						if(pathname.getName().endsWith(".flv") && pathname.getName().length() == 5){
							pathname.renameTo(new File(pathname.getParentFile().getAbsolutePath() + File.separator + "0" + pathname.getName()));
						}
						
						//0.blv.bdl -- 00.blv.bdl
						if(pathname.getName().endsWith(".blv.bdl") && pathname.getName().length() == 9){
							pathname.renameTo(new File(pathname.getParentFile().getAbsolutePath() + File.separator + "0" + pathname.getName()));
						}
					}
					
					//过滤得到视频文件
					ffs = ff.listFiles(new FileFilter() {
							
						public boolean accept(File pathname) {
							for(int i = 0;i < ff.list().length;i++){
								if(pathname.getName().endsWith(".blv") || pathname.getName().endsWith(".flv") || pathname.getName().endsWith(".blv.bdl")){
									return true;
								}
							}
							return false;
						}
					});
					
					//输出路径
					episode_path = out.getAbsolutePath() + File.separator + names[0];
					File episode = new File(episode_path);
					if(!episode.exists()){
						episode.mkdirs();
					}
						
					//合并
					System.out.println("开始合并" + episode_path);
					FlvMerge mFlvMerge = new FlvMerge();
					try {
						mFlvMerge.merge(ffs, new File(episode_path + File.separator + names[1] + ".flv"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public String nameMatch(String str){
		String[] fbsArr = { "\\", "<", ">", "/", "\"",  "[", "]", "?", "^", "*", "|" };  
        for (String key : fbsArr) {  
            if (str.contains(key)) {  
            	str = str.replace(key, "-");  
            }  
        } 
		return str;
	}
	
}
